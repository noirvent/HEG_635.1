package etu.p16.s04.f1.base;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import android.content.res.Resources;
import etu.p16.s04.f1.R;
import etu.p16.s04.f1.domaine.Pays;
import etu.p16.s04.f1.domaine.Pilote;

/**
 * 635.1 - TP S04
 * 
 * Chargement des donn�es.
 * 
 * Format des fichiers:
 * - pays.txt:     siglePays;nomPays
 * - pilotes.txt:  nom;pr�nom;photo;dateNaissance;siglePays;rang2012;points2012;rang2013;points2013;rang2014;points2014;rang2015;points2015
 * 
 * Remarques:
 * - siglePays est l'identifiant d'un pays; c'est �galement le nom du fichier contenant le drapeau du pays.
 * - photo est le nom du fichier contenant la photo du coureur
 * - si, pour une ann�e AAAA donn�e, le r�sultat du coureur n'est pas d�fini, les valeurs de rangAAAA et pointsAAAA sont �gales � -1.
 * 
 * @author BLUM Jonathan
 * @version Version 1.0
*/
public class Dao {

  private static final int EOF = -1;                    /* Marque de fin de fichier */
  private static final String FORM_DATE = "dd.MM.yyyy"; /* Format de la date de naissance des pilotes */
  private static final ArrayList<Pays> lstpays = new ArrayList<Pays>();
    
  /* Retourne la r�f�rence de R.drawable de l'image de nom nomImage */
  private static int refImage (Resources res, String nomImage) {
    return res.getIdentifier(nomImage.toLowerCase(Locale.getDefault()), "drawable", R.class.getPackage().getName());
  } // refImage
  
  private static int drapeau(Resources res, String sigle) {
	  return res.getIdentifier(sigle.toLowerCase(Locale.getDefault()), "drawable", R.class.getPackage().getName());
  }

  /* Lecture de l'InputStream in et retour de la s�quence de charact�res dans un String */
  private static String lireStr (InputStream in) {
    StringBuilder b = new StringBuilder();
    try {
      b.ensureCapacity(in.available() + 10);
      int c = in.read();
      while (c != EOF) {b.append((char)c); c = in.read();}
      in.close();
    }
    catch (IOException e) {e.printStackTrace();}
    return b.toString();
  } // lireStr
  
  public static void initPays(Resources res) {
	  String[] paysstr= lireStr(res.openRawResource(R.raw.pays)).split("\r\n");
	  for(String line: paysstr) {
		  String[] paysval = line.split(";");
		  lstpays.add(new Pays(paysval[0], paysval[1], drapeau(res,paysval[0])));
	  }
	  
  }
  /* Cherche et retourne un pays dans la liste des pays */
  public static Pays getPays(String pays) {
	  Pays paystofind = new Pays(pays, "", -1);
	  if(lstpays.contains(paystofind)) {
		  return lstpays.get(lstpays.indexOf(paystofind));
	  }
	  return null;
  }
  
  public static ArrayList<Pilote> getPiloteByYear(Resources res, final int year) {
	  ArrayList<Pilote> pilotefromyear = new ArrayList<Pilote>();
	  String[] pilotestr = lireStr(res.openRawResource(R.raw.pilotes)).split("\r\n");
	  for(String line: pilotestr) {
		  String[] piloteval = line.split(";");
		  SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyy");
		  try {
			Date date = sdf.parse(piloteval[3]);
			Pilote p = new Pilote(piloteval[0], piloteval[1], refImage(res, piloteval[2]), date, getPays(piloteval[4]));
			p.setRang(2012,  Integer.parseInt(piloteval[5]));
			p.setScore(2012, Integer.parseInt(piloteval[6]));
			p.setRang(2013,  Integer.parseInt(piloteval[7]));
			p.setScore(2013, Integer.parseInt(piloteval[8]));
			p.setRang(2014,  Integer.parseInt(piloteval[9]));
			p.setScore(2014, Integer.parseInt(piloteval[10]));
			p.setRang(2015,  Integer.parseInt(piloteval[11]));
			p.setScore(2015, Integer.parseInt(piloteval[12]));
			
			if(p.getRang(year) > -1)
				pilotefromyear.add(p);
			
		  } catch (ParseException e) { e.printStackTrace(); }		  
	  }
	  
	  return pilotefromyear;
  }
  
  
  
} // Dao