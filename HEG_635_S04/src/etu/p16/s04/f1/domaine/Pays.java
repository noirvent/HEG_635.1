package etu.p16.s04.f1.domaine;

/**
 * 635.1 - TP S04
 * 
 * Classe repr�sentant un pays.
 * 
 * @author Jonathan Blum
 * @version Version 1.0
*/
public class Pays {
  
  private String sigle; /* Sigle identifant le pays */
  private String nom;   /* Nom du pays */
  private int drapeau;  /* Identifiant du drapeau du pays */
  
  /** Constructeur */
  public Pays (String sigle, String nom, int drapeau) {this.sigle = sigle; this.nom = nom; this.drapeau = drapeau;}

  /** Accesseurs */
  public String getSigle () {return sigle;}
  public String getNom () {return nom;}
  public int getDrapeau () {return drapeau;}

  @Override
  public boolean equals (Object obj) {return ((Pays)obj).sigle.equals(sigle);}
  @Override
  public String toString () {return "[" + sigle + "] " + nom;}
  
} // Pays