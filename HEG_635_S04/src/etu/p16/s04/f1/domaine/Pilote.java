package etu.p16.s04.f1.domaine;

import android.annotation.SuppressLint;
import java.util.Date;
import java.util.HashMap;
/**
 * 635.1 - TP S04
 * 
 * Classe repr�sentant un pilote.
 * 
 * Identifiant: nom et pr�nom
 * 
 * @author Jonathan BLUM
 * @version Version 1.0
*/
public class Pilote {
    
  private String nom;                           /* Nom du pilote */
  private String prenom;                        /* Pr�nom du pilote */
  private int photo;                            /* Identifiant de la photo du pilote */
  private Date date;                            /* Date de naissance du pilote */
  private Pays pays;                            /* Pays d'origine du pilote */
  @SuppressLint("UseSparseArrays")
  private HashMap<Integer, Integer> scorebyyear = new HashMap<Integer,Integer>();
  @SuppressLint("UseSparseArrays")
private HashMap<Integer, Integer> rangbyyear = new HashMap<Integer,Integer>();
  
  
  /** Constructeur */
  public Pilote (String nom, String prenom, int photo, Date date, Pays pays) {
    this.nom = nom; this.prenom = prenom;
    this.photo = photo; this.date = date; this.pays = pays;
  } // Constructeur

  /** Accesseurs */
  public String getNom () {return nom;}
  public String getPrenom () {return prenom;}
  public int getPhoto () {return photo;}
  public Date getDate () {return date;}
  public Pays getPays () {return pays;}
  public int getScore(int year) {return scorebyyear.get(year); }
  public int getRang(int year) {return rangbyyear.get(year); }
  public void setScore(int year, int score) { scorebyyear.put(year, score); }
  public void setRang(int year, int score) { rangbyyear.put(year, score); }
  
  
  @Override
  public boolean equals (Object obj) {
    Pilote p = (Pilote)obj;
    return p.nom.equals(nom) && p.prenom.equals(prenom);
  } // equals
  
  @Override
  public String toString () {return nom + " " + prenom;}

} // Pilote