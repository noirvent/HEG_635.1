package etu.p16.s04.f1.presentation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import etu.p16.s04.f1.R;
import etu.p16.s04.f1.base.Dao;
import etu.p16.s04.f1.domaine.Pilote;

/**
 * 635.1 - TP S04
 * 
 * Application de consultation des palmar�s du championnat du monde des conducteurs de F1 pour les ann�es 2012-1015.
 * 
 * - S�lection d'un classement � afficher (2012, 2013, 2014 ou 2015) par choix du menu.
 * - Affichage du classement correspondant.
 * - La s�lection d'un coureur affiche la liste des informations d�taill�es le concernant.
 *
 * @author 
 * @version Version 1.0 
*/
public class Formule1MainActivity extends Activity {

  private static final String FORM_DATE = "dd.MM.yyyy"; /* Format de la date de naissance des pilotes */
  private static final String[] FROM = {"nom-prenom", "rank", "drapeau"};
  private static final int[] TO = {R.id.tvNomPrenom, R.id.tvRang, R.id.imDrapeau};

  /* Contr�les */
  private LinearLayout layLogo, layPilote, layClassement;
  private TextView tvNom, tvPrenom, tvDateNaissance, tvRes2012, tvRes2013, tvRes2014, tvRes2015, tvTitre;
  private ListView lvClassement;
  private ImageView imPilote;
  
  private int currentYear = 0;

  private void definirControles () {
    /* Logo */
    layLogo = (LinearLayout)findViewById(R.id.layLogo);
    /* Informations d'un pilote */
    layPilote = (LinearLayout)findViewById(R.id.layPilote);
    imPilote = (ImageView)findViewById(R.id.imPilote);
    tvNom = (TextView)findViewById(R.id.tvNom);
    tvPrenom = (TextView)findViewById(R.id.tvPrenom);
    tvDateNaissance = (TextView)findViewById(R.id.tvDateNaissance);
    tvRes2012 = (TextView)findViewById(R.id.tvRes2012);
    tvRes2013 = (TextView)findViewById(R.id.tvRes2013);
    tvRes2014 = (TextView)findViewById(R.id.tvRes2014);
    tvRes2015 = (TextView)findViewById(R.id.tvRes2015);
    /* Classement des pilotes */
    layClassement = (LinearLayout)findViewById(R.id.layClassement);
    tvTitre = (TextView)findViewById(R.id.tvTitre);
    lvClassement = (ListView)findViewById(R.id.lvClassement);
  } // definirControles
    
  @Override
  protected void onCreate (Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    definirControles();
	  displayControls(false, false);
    
  } // onCreate
  
  
  public boolean onCreateOptionsMenu(Menu menu) {
	  MenuInflater inflater = getMenuInflater();
	  inflater.inflate(R.menu.menu, menu);
	  return true;
  }
  public boolean onPrepareOptionsMenu(Menu menu) {
	  MenuItem it2012 = menu.findItem(R.id.it2012);
	  MenuItem it2013 = menu.findItem(R.id.it2013);
	  MenuItem it2014 = menu.findItem(R.id.it2014);
	  MenuItem it2015 = menu.findItem(R.id.it2015);
	  it2012.setEnabled(currentYear != 2012);
	  it2013.setEnabled(currentYear != 2013);
	  it2014.setEnabled(currentYear != 2014);
	  it2015.setEnabled(currentYear != 2015);
	  
	return true;
	  
  }
  
  public boolean onOptionsItemSelected(MenuItem item) {
	  switch (item.getItemId()) {
		case R.id.it2012:
			currentYear=2012;
			break;
		case R.id.it2013:
			currentYear=2013;
			break;
		case R.id.it2014:
			currentYear=2014;
			break;
		case R.id.it2015:
			currentYear=2015;
			break;
		default:
			return super.onOptionsItemSelected(item);
	  }
	  updateList();
	  return true;
  }
  
  public void updateList() {
	  invalidateOptionsMenu();
	  Dao.initPays(getResources());
	  
	  final ArrayList<Pilote> participants = Dao.getPiloteByYear(getResources(), currentYear);
	  
	  //Sort participants
	  Collections.sort(participants, new Comparator<Pilote>() {
		@Override public int compare(Pilote p1, Pilote p2) {			
			return p1.getRang(currentYear) - p2.getRang(currentYear);
		}
	  });
	   
	  // Generate Classement
	  List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>(participants.size());
	  
	  for(Pilote p : participants) {
		  HashMap<String, Object> map = new HashMap<String, Object>();
		  map.put(FROM[0], p.getNom() + " " + p.getPrenom());
		  map.put(FROM[1], p.getRang(currentYear));
		  map.put(FROM[2], p.getPays().getDrapeau());
		  data.add(map);
	  }
	  
	  SimpleAdapter adapter= new SimpleAdapter(getApplicationContext(), data, R.layout.un_pilote, FROM, TO);
	  
	  // Display Classement
	  tvTitre.setText(getResources().getString(R.string.classement) +" "+ currentYear);
	  lvClassement.setOnItemClickListener(new OnItemClickListener() {
		@Override public void onItemClick(AdapterView<?> adapter, View view, int pos, long arg3) {
			Pilote p = participants.get(pos);
			selectPilote(p);
	    }
	  });
	  
	  lvClassement.setAdapter(adapter);	  
	  displayControls(false, true);
	  
  }
  
  private void selectPilote(Pilote p) {	
		tvNom.setText(p.getNom());
		tvPrenom.setText(p.getPrenom());
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		tvDateNaissance.setText(sdf.format(p.getDate()));
		tvRes2012.setText(p.getRang(2012) > -1 ? p.getRang(2012)+"e (" + p.getScore(2012)+"pts)":"-");
		tvRes2013.setText(p.getRang(2013) > -1 ? p.getRang(2013)+"e (" + p.getScore(2013)+"pts)":"-");
		tvRes2014.setText(p.getRang(2014) > -1 ? p.getRang(2014)+"e (" + p.getScore(2014)+"pts)":"-");
		tvRes2015.setText(p.getRang(2015) > -1 ? p.getRang(2015)+"e (" + p.getScore(2015)+"pts)":"-");
		System.out.println("nuff" + p.getPhoto());
		imPilote.setImageResource(p.getPhoto());
		displayControls(true, false);
  }
  
  public void retour(View view) {
	  displayControls(false,  true);
  }
  
  public void displayControls(boolean pilote, boolean classement) {
	  int on = View.VISIBLE;
	  int off = View.GONE;
	  
	  layPilote.setVisibility(pilote ? on : off);
	  layClassement.setVisibility(classement ? on : off);
	  layLogo.setVisibility(pilote || classement ? off : on);
  }

} // Formule1MainActivity
