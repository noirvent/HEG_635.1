/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package etu.p16.s06.automobiles;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int colBlack=0x7f070000;
        public static final int colGray=0x7f070001;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
    }
    public static final class drawable {
        public static final int alfa_romeo=0x7f020000;
        public static final int all=0x7f020001;
        public static final int ang=0x7f020002;
        public static final int aston_martin=0x7f020003;
        public static final int audi=0x7f020004;
        public static final int bmw=0x7f020005;
        public static final int bmw_alpina=0x7f020006;
        public static final int chevrolet=0x7f020007;
        public static final int citroen=0x7f020008;
        public static final int cor=0x7f020009;
        public static final int dacia=0x7f02000a;
        public static final int div=0x7f02000b;
        public static final int divers=0x7f02000c;
        public static final int esp=0x7f02000d;
        public static final int fiat=0x7f02000e;
        public static final int filtre=0x7f02000f;
        public static final int ford=0x7f020010;
        public static final int fra=0x7f020011;
        public static final int hon=0x7f020012;
        public static final int honda=0x7f020013;
        public static final int hyundai=0x7f020014;
        public static final int ic_launcher=0x7f020015;
        public static final int infiniti=0x7f020016;
        public static final int ita=0x7f020017;
        public static final int jaguar=0x7f020018;
        public static final int jap=0x7f020019;
        public static final int jeep=0x7f02001a;
        public static final int kia=0x7f02001b;
        public static final int lancia=0x7f02001c;
        public static final int land_rover=0x7f02001d;
        public static final int lexus=0x7f02001e;
        public static final int maserati=0x7f02001f;
        public static final int mazda=0x7f020020;
        public static final int mercedes=0x7f020021;
        public static final int mini=0x7f020022;
        public static final int mitsubishi=0x7f020023;
        public static final int nissan=0x7f020024;
        public static final int opel=0x7f020025;
        public static final int peugeot=0x7f020026;
        public static final int porsche=0x7f020027;
        public static final int renault=0x7f020028;
        public static final int rou=0x7f020029;
        public static final int seat=0x7f02002a;
        public static final int skoda=0x7f02002b;
        public static final int smart=0x7f02002c;
        public static final int ssangyong=0x7f02002d;
        public static final int subaru=0x7f02002e;
        public static final int sue=0x7f02002f;
        public static final int suzuki=0x7f020030;
        public static final int tch=0x7f020031;
        public static final int toyota=0x7f020032;
        public static final int usa=0x7f020033;
        public static final int volkswagen=0x7f020034;
        public static final int volvo=0x7f020035;
    }
    public static final class id {
        public static final int etDetailMarque=0x7f090007;
        public static final int etFiltre=0x7f090004;
        public static final int etVentes2013=0x7f090009;
        public static final int etVentes2014=0x7f09000a;
        public static final int imDetailLogo=0x7f090006;
        public static final int imDetailPays=0x7f090008;
        public static final int imLogo=0x7f09000c;
        public static final int imPays=0x7f09000e;
        public static final int layDetail=0x7f090005;
        public static final int layFiltre=0x7f090000;
        public static final int lvMarques=0x7f09000b;
        public static final int rbMarques=0x7f090002;
        public static final int rbPays=0x7f090003;
        public static final int rgFiltre=0x7f090001;
        public static final int tvMarque=0x7f09000d;
        public static final int tvPays=0x7f09000f;
    }
    public static final class layout {
        public static final int main=0x7f030000;
        public static final int une_marque=0x7f030001;
    }
    public static final class raw {
        public static final int pays=0x7f040000;
        public static final int ventes=0x7f040001;
    }
    public static final class string {
        public static final int app_name=0x7f060000;
        public static final int libDetail=0x7f060005;
        public static final int libEntrez=0x7f060004;
        public static final int libFiltre=0x7f060001;
        public static final int libMarques=0x7f060002;
        public static final int libPays=0x7f060003;
        public static final int libVentes2013=0x7f060006;
        public static final int libVentes2014=0x7f060007;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f080000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f080001;
    }
}
