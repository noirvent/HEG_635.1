package etu.p16.s06.automobiles.domaine;

/**
 * 635.1 - TP S06
 *
 * Application de consultation des ventes en Suisse des diff�rentes marques automobiles en 2013 et 2014
 *
 * Mod�lisation d'une marque.
 *
 * @author VOTRE NOM
 */
public class Marque implements Comparable<Marque> {

  private int id;         /* Identifiant */
  private String nom;     /* Nom de la marque */
  private String nomlowercase;
  private int ventes2013; /* Nombre de ventes en 2013 */
  private int ventes2014; /* Nombre de ventes en 2014 */
  private int logo;       /* Identifiant de l'image représentant le logo de la marque */
  private Pays pays;      /* Pays de la marque */
  
  /** Constructeur */
  public Marque (int id, String nom, int ventes2013, int ventes2014, int logo, Pays pays) {
    this.id = id;
    this.nom = nom;
    this.ventes2013 = ventes2013; this.ventes2014 = ventes2014;
    this.logo = logo; this.pays = pays;
    this.nomlowercase = nom.toLowerCase();
  } // Constructeur

  /* Accesseurs */
  public int getId () {return id;}
  public String getNom () {return nom;}
  public String getNomLowerCase() {return nomlowercase;}
  public int getVentes2013 () {return ventes2013;}
  public int getVentes2014 () {return ventes2014;}
  public int getLogo () {return logo;}
  public Pays getPays () {return pays;}
  
  /** Deux marques sont �gales lorsque leurs identifiants sont identiques */
  @Override
  public boolean equals (Object o) {return ((Marque)o).id == id;}

  @Override
  public int compareTo (Marque m) {
    return nom.compareTo(m.getNom());
  } // compareTo

@Override
public String toString() {
	return "Marque [nom=" + nom + "]";
}
  
  
  
} // Marque
