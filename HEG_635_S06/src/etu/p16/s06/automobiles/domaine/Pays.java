package etu.p16.s06.automobiles.domaine;

/**
 * 635.1 - TP S06
 *
 * Application de consultation des ventes en Suisse des diff�rentes marques automobiles en 2013 et 2014
 *
 * Modélisation d'un pays.
 *
 * @author Jonathan Blum
 */
public class Pays {
  
  private String id;   /* Identifiant */
  private String nom;  /* Nom du pays */
  private String nomlowercase;
  private int drapeau; /* Identifiant de l'image repr�sentant le drapeau du pays */
  
  /** Constructeur */
  public Pays (String id, String nom, int drapeau) {this.id = id; this.nom = nom; this.drapeau = drapeau; this.nomlowercase = nom.toLowerCase();}

  /** Accesseurs */
  public String getId () {return id;}
  public String getNom () {return nom;}
  public String getNomLowerCase() { return nomlowercase; }
  public int getDrapeau () {return drapeau;}
  
  /** Deux pays sont égaux lorsque leurs identifiants sont identiques */
  @Override
  public boolean equals (Object o) {return ((Pays)o).id.equals(id);}

	@Override
	public String toString() {return "Pays [id=" + id + ", nom=" + nom + "]";}
	  
  

} // Pays
