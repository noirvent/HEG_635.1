package etu.p16.s06.automobiles.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.TreeSet;

import android.content.res.Resources;
import etu.p16.s06.automobiles.R;
import etu.p16.s06.automobiles.domaine.Marque;
import etu.p16.s06.automobiles.domaine.Pays;

/**
 * 635.1 - TP S06
 *
 * Application de consultation des ventes en Suisse des différentes marques automobiles en 2013 et 2014
 *
 * Chargement des donnes.
 *
 * @author Jonathan Blum
 */
public class Data {

  private static final int EOF = -1; /* Marque de fin de fichier */
  
  private static int refImage (Resources res, String nomImage) {
	  return res.getIdentifier(nomImage, "drawable", R.class.getPackage().getName());
  } // refImage
  
  /* Lecture de l'InputStream in et retour de la s�quence de charact�res dans un String */
  private static String lireStr (InputStream in) {
    StringBuilder b = new StringBuilder();
    try {
      b.ensureCapacity(in.available() + 10);
      int c = in.read();
      while (c != EOF) {b.append((char)c); c = in.read();}
      in.close();
    }
    catch (IOException e) {e.printStackTrace();}
    return b.toString();
  } // lireStr
  
  public static TreeSet<Marque> getMarques(Resources res, HashMap<String, Pays> pays) {
	String[] rawMarques = lireStr(res.openRawResource(R.raw.ventes)).split("\r\n");
	TreeSet<Marque> marques = new TreeSet<Marque>();
	for(String lineMarque : rawMarques) {
		String[] val = lineMarque.split(";");
		int id = Integer.parseInt(val[0]);
		int v2013 = Integer.parseInt(val[2]);
		int v2014 = Integer.parseInt(val[3]);		
		marques.add(new Marque(id, val[1], v2013, v2014, refImage(res, val[4]), pays.get(val[5])));
	}
	  
	return marques;
	  
  }
  
  public static HashMap<String, Pays> getPays(Resources res) {
	  String[] rawPays = lireStr(res.openRawResource(R.raw.pays)).split("\r\n");
	  HashMap<String, Pays> allPays = new HashMap<String, Pays>(rawPays.length*2);
	  for(String linePays: rawPays) {
		  String[] val = linePays.split(";");
		  allPays.put(val[0], new Pays(val[0], val[1], refImage(res, val[0])));
	  }
	  
	  return allPays;
	  
  }
  
} // Data
