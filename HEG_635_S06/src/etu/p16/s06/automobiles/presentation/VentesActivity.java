package etu.p16.s06.automobiles.presentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import etu.p16.s06.automobiles.R;
import etu.p16.s06.automobiles.base.Data;
import etu.p16.s06.automobiles.domaine.Marque;
import etu.p16.s06.automobiles.domaine.Pays;
/**
 * 635.1 - TP S06
 *
 * Application de consultation des ventes en Suisse des diff�rentes marques automobiles en 2013 et 2014
 *
 * La liste alphab�tique des constructeurs est affich�e.
 * Il est possible de filtrer la liste affich�e selon la marque ou selon le pays.
 * Le r�sultat su filtrage s'affiche au fur et � mesure de la frappe (en temps r�el).
 * La s�lection d'une marque de la liste affiche les informations d�taill�es la concernant.
 *
 * @author Jonathan Blum
 */
public class VentesActivity extends Activity {
	
	  private static final int ETAT_FILTRE = 0;  /* état Filtre */
	  private static final int ETAT_DETAIL = 1;    /* Le détail est affiché */
	  private static final String MARQUE = "marque";
	  
	  	
  private static final String[] FROM = {"nom", "logo", "paysnom", "payslogo"};
  private static final int[] TO = {R.id.tvMarque, R.id.imLogo, R.id.tvPays, R.id.imPays};
  
  /* Contrôles de l'écran */
  private LinearLayout layFiltre, layDetail;
  private RadioGroup rgFiltre;
  private RadioButton rbMarques, rbPays;
  private ImageView imDetailLogo, imDetailPays;
  private EditText etFiltre, etDetailMarque, etVentes2013, etVentes2014;
  private ListView lvMarques;
  
  private HashMap<String, Pays> pays = new HashMap<String, Pays>(); 
  private TreeSet<Marque> marques = new TreeSet<Marque>(); 
  

  private void definirVariables () {
    /* Filtre */
    layFiltre = (LinearLayout)findViewById(R.id.layFiltre);
    rgFiltre = (RadioGroup)findViewById(R.id.rgFiltre);
    rbMarques = (RadioButton)findViewById(R.id.rbMarques);
    rbPays = (RadioButton)findViewById(R.id.rbPays);
    etFiltre = (EditText)findViewById(R.id.etFiltre);
    /* Détail */
    layDetail = (LinearLayout)findViewById(R.id.layDetail);
    imDetailLogo = (ImageView)findViewById(R.id.imDetailLogo);
    etDetailMarque = (EditText)findViewById(R.id.etDetailMarque);
    imDetailPays = (ImageView)findViewById(R.id.imDetailPays);
    etVentes2013 = (EditText)findViewById(R.id.etVentes2013);
    etVentes2014 = (EditText)findViewById(R.id.etVentes2014);
    /* Liste */
    lvMarques = (ListView)findViewById(R.id.lvMarques);
  } // definirVariables
  
  private void definirListeners() {
	  etFiltre.addTextChangedListener(new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) { filtrerList(s.toString()); }
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
		
		@Override
		public void afterTextChanged(Editable s) { }
	});
	  
    lvMarques.setOnItemClickListener(new OnItemClickListener() {
        public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
          @SuppressWarnings("unchecked") HashMap<String, Object> hm = (HashMap<String, Object>)parent.getItemAtPosition(position);
          afficheMarque((Marque)hm.get(MARQUE));
        } // onItemClick
      });   
  }
  
  
  private void initialiser() {
	  
	 rbMarques.setChecked(true);
	 Resources res = getResources();

     pays = Data.getPays(res);
     marques = Data.getMarques(res, pays);
     filtrerList("");
	 
  }
  
  private void afficheMarque(Marque m) {
	  Log.d("MarquePays", m.getPays().toString());
	  etDetailMarque.setText(m.getNom());
	  imDetailLogo.setImageResource(m.getLogo());
	  imDetailPays.setImageResource(m.getPays().getDrapeau());
	  
	  etVentes2013.setText(Integer.toString(m.getVentes2013()));
	  etVentes2014.setText(Integer.toString(m.getVentes2014()));
	  setEtat(ETAT_DETAIL);
  }
  
  private void filtrerList(String filtre) {
	  boolean filterByPays = rbPays.isChecked();
	  	filtre = filtre.toLowerCase();
	  	if(filtre.equals("")) {
			Log.d("Filtering", "Do not Filter");
		  lvMarques.setAdapter(getMarquesAdapter(marques));
	  	} else {
			Log.d("Filtering", "Filter with " + filtre);
	  		TreeSet<Marque> filteredMarques = new TreeSet<Marque>();
	  		for(Marque m: marques) {

	  			if(filterByPays && m.getPays().getNomLowerCase().startsWith(filtre))
	  				filteredMarques.add(m);
	  			else if(!filterByPays && m.getNomLowerCase().startsWith(filtre))
	  				filteredMarques.add(m);
	  		}
	  		lvMarques.setAdapter(getMarquesAdapter(filteredMarques));
	  	}
  }
  
  public SimpleAdapter getMarquesAdapter(TreeSet<Marque> filteredMarques) {
	  List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>(filteredMarques.size());
		for(Marque m: filteredMarques) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(FROM[0], m.getNom());
			map.put(FROM[1], m.getLogo());
			map.put(FROM[2], m.getPays().getNom());
			map.put(FROM[3], m.getPays().getDrapeau());
			map.put(MARQUE, m);
			data.add(map);
		}
		return new SimpleAdapter(getApplicationContext(), data, R.layout.une_marque, FROM, TO);
  }

  public void onRadioButtonClicked(View view) {
	  
  }
  
  
  @Override
  protected void onCreate (Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    definirVariables();
    definirListeners();    
    initialiser();
    setEtat(ETAT_FILTRE);
  } // onCreate
  
  
  public void showFiltre(View v) {
	  setEtat(ETAT_FILTRE);
  }
  
  private void setEtat (int etat) {
	    switch (etat) {
	      case ETAT_FILTRE:
	        layFiltre.setVisibility(View.VISIBLE); layDetail.setVisibility(View.GONE);
	        break;
	      case ETAT_DETAIL:
		    layFiltre.setVisibility(View.GONE); layDetail.setVisibility(View.VISIBLE);
	        break;
	    }
  }
} // VentesActivity
