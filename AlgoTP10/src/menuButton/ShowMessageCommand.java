/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menuButton;

/**
 *
 * @author Jonathan Blum <jonathan.blum@eldhar.com>
 */
public class ShowMessageCommand implements Command {
    private final MenuButtonView parentView;
    private final int messageid;

    ShowMessageCommand(MenuButtonView parent, int messageid) {
        this.parentView = parent;
        this.messageid = messageid;
    }

    @Override
    public void execute() {
        switch(messageid) {
            case 1: parentView.afficherUn(); break;
            case 2: parentView.afficherDeux(); break;
            case 3: parentView.afficherTrois(); break;
        }
    }
    
}
