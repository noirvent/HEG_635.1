/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menuButton;

/**
 *
 * @author Jonathan Blum <jonathan.blum@eldhar.com>
 */
public interface Commandable {

    Command getCommand();

    void setCommand(Command command);
    
}
