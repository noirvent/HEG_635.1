/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menuButton;

import javax.swing.JMenuItem;

/**
 *
 * @author Jonathan Blum <jonathan.blum@eldhar.com>
 */
public class JMenuItemCommandable extends JMenuItem implements Commandable {
    private Command command;

    JMenuItemCommandable(String nom) {
        super(nom);
    }

    @Override
    public Command getCommand() {
        return command;
    }

    @Override
    public void setCommand(Command command) {
        this.command = command;
    }
    
    
}
