package menuButton;

import java.awt.event.*;
import java.util.LinkedList;

import javax.swing.*;


public class MenuButtonView extends JFrame implements ActionListener {
        
        LinkedList<Command> queue = new LinkedList<>();
	public MenuButtonView() {
		setResizable(false);
		setTitle("MenuButtonView");
		getContentPane().setLayout(null);
		setBounds(100, 100, 280, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
	    JMenu menu = new JMenu("Menu");
	    menuBar.add(menu);
	    JMenuItemCommandable  menuItem = new JMenuItemCommandable("Item1");
            menuItem.setCommand(new ShowMessageCommand(this, 1));
	    menuItem.addActionListener(this);
	    menu.add(menuItem);
	    menuItem = new JMenuItemCommandable("Item2");
            menuItem.setCommand(new ShowMessageCommand(this, 2));
	    menuItem.addActionListener(this);
	    menu.add(menuItem);
	    menuItem = new JMenuItemCommandable("Item3");
            menuItem.setCommand(new ShowMessageCommand(this, 3));
	    menuItem.addActionListener(this);
	    menu.add(menuItem);
	    setJMenuBar(menuBar);
	    
		JButton rejouer = new JButton();
		rejouer.setActionCommand("rejouer");
                rejouer.setText("Rejouer");
		rejouer.setBounds(80, 22, 100, 20);
		rejouer.addActionListener(this);
		getContentPane().add(rejouer);
		
		setVisible(true);
	}
        @Override
	public void actionPerformed(ActionEvent e){
            if(e.getActionCommand().equals("rejouer"))
                rejouer();
            else if(e.getSource() instanceof Commandable) 
                addCommandToQueue(((Commandable)e.getSource()).getCommand());
	}
        
        void addCommandToQueue(Command command) {
            System.out.println("Store Command");
            queue.addLast(command);
        }
	
	void rejouer(){
            if(!queue.isEmpty())
		((Command)queue.removeLast()).execute();
	}
        
	void afficherUn(){ JOptionPane.showMessageDialog(this, "Item1");}
	void afficherDeux(){JOptionPane.showMessageDialog(this, "Item2");}
	void afficherTrois(){JOptionPane.showMessageDialog(this, "Item3");}
	
}