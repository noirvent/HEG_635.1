package etu.p16.cc1.plan.modulaire.domaine;

/**
 * 635.1 - Contr�le continu du 15.04.2016
 *
 * Application de consultation du plan modulaire de la HEG
 * 
 * Repr�sentation d'un module.
 *
 * @author Peter DAEHNE - HEG-Gen�ve
 * @version Version 1.0
 */
public class Module implements Comparable<Module> {
  
  private String numero;       /* Num�ro du module: identifiant */
  private String nom;          /* Nom du module */
  private int credits;         /* Nombre de cr�dits */
  private int semPleinTemps;   /* Num�ro du semestre o� le module est donn� aux �tudiants � plein temps */
  private int semTempsPartiel; /* Num�ro du semestre o� le module est donn� aux �tudiants � temps partiel */

  /** Constructeur */
  public Module (String numero, String nom, int credits, int semPleinTemps, int semTempsPartiel) {
    this.numero = numero; this.nom = nom; this.credits = credits;
    this.semPleinTemps = semPleinTemps; this.semTempsPartiel = semTempsPartiel;
  } // Constructeur

  public String getNumero () {return numero;}
  public String getNom () {return nom;}
  public int getCredits () {return credits;}
  public int getSemPleinTemps () {return semPleinTemps;}
  public int getSemTempsPartiel () {return semTempsPartiel;}
  
  @Override
  public boolean equals (Object obj) {return ((Module)obj).numero.equals(numero);}

  @Override
  /** Ordre croissant des num�ros de module */
  public int compareTo (Module m) {return numero.compareTo(m.numero);}

} // Module
