package etu.p16.cc1.plan.modulaire.domaine;

/**
 * 635.1 - Contr�le continu du 15.04.2016
 *
 * Application de consultation du plan modulaire de la HEG
 * 
 * Repr�sentation d'une unit� de cours.
 *
 * @author Peter DAEHNE - HEG-Gen�ve
 * @version Version 1.0
 */
public class UniteCours implements Comparable<UniteCours> {
  
  private String numero; /* Num�ro du module dont le cours fait partie */
  private String nom;    /* Nom du cours: identifiant */
  private int heures;    /* Nombre d'heures de cours par semaine */

  /** Constructeur */
  public UniteCours (String numero, String nom, int heures) {
    this.numero = numero; this.nom = nom; this.heures = heures;
  } // Constructeur

  public String getNumero () {return numero;}
  public String getNom () {return nom;}
  public int getHeures () {return heures;}
  
  @Override
  public boolean equals (Object obj) {return ((UniteCours)obj).nom.equals(nom);}

  @Override
  /** Ordre croissant des nom de cours */
  public int compareTo (UniteCours uc) {return nom.compareTo(uc.nom);}

} // UniteCours
