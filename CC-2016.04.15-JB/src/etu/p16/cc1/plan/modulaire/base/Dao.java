package etu.p16.cc1.plan.modulaire.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.TreeSet;

import android.content.res.Resources;
import etu.p16.cc1.plan.modulaire.R;
import etu.p16.cc1.plan.modulaire.domaine.Module;
import etu.p16.cc1.plan.modulaire.domaine.UniteCours;

/**
 * 635.1 - Contr�le continu du 15.04.2016
 *
 * Application de consultation du plan modulaire de la HEG
 * 
 * Chargement des donn�es.
 * 
 * Format des fichiers:
 * - module.txt: n�mod;nom;credits;semPT;semTP
 * - uc.txt:     n�mod;nom;nbHeures
 * 
 * @author Jonathan Blum - Num�ro du poste: HEG-WS-8139
 * @version Version 1.0
*/
public class Dao {

  private static final int EOF = -1; /* Marque de fin de fichier */
  
  /* Lecture de l'InputStream in et retour de la s�quence de charact�res dans un String */
  private static String lireStr (InputStream in) {
    StringBuilder b = new StringBuilder();
    try {
      b.ensureCapacity(in.available() + 10);
      int c = in.read();
      while (c != EOF) {b.append((char)c); c = in.read();}
      in.close();
    }
    catch (IOException e) {e.printStackTrace();}
    return b.toString();
  } // lireStr
  
  public static HashMap<String, Module> getModules(Resources res) {
    HashMap<String, Module> hmModules = new HashMap<String, Module>();
    String[] rawModules = lireStr(res.openRawResource(R.raw.module)).split("\r\n");
    for(String line: rawModules) {
      String[] vals = line.split(";");
      int credits = Integer.parseInt(vals[2]);
      int semPT = Integer.parseInt(vals[3]);
      int semTP = Integer.parseInt(vals[4]);
      hmModules.put(vals[0], new Module(vals[0], vals[1], credits, semPT, semTP));
    }
    return hmModules;
  }
  
  public static HashMap<String,TreeSet<UniteCours>> getUnitecours(Resources res) {
    HashMap<String, TreeSet<UniteCours>> hmUC = new HashMap<String, TreeSet<UniteCours>>();
    //
    String[] rawUC = lireStr(res.openRawResource(R.raw.uc)).split("\r\n");
    for(String line: rawUC) {
      String[] vals = line.split(";");
      int heures = Integer.parseInt(vals[2]);
      UniteCours uc = new UniteCours(vals[0], vals[1], heures);
      
      if(hmUC.containsKey(vals[0])) {
        hmUC.get(vals[0]).add(uc);
      } else {
        TreeSet<UniteCours> tsUC = new TreeSet<UniteCours>();
        tsUC.add(uc);
        hmUC.put(vals[0], tsUC);
      }
    }
    return hmUC;
  }
    
} // Dao
