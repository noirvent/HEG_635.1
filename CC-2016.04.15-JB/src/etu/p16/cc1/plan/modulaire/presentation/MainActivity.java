package etu.p16.cc1.plan.modulaire.presentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import org.w3c.dom.Text;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import etu.p16.cc1.plan.modulaire.R;
import etu.p16.cc1.plan.modulaire.base.Dao;
import etu.p16.cc1.plan.modulaire.domaine.Module;
import etu.p16.cc1.plan.modulaire.domaine.UniteCours;

/**
 * 635.1 - Contr�le continu du 15.04.2016
 *
 * Application de consultation du plan modulaire de la HEG
 * - L'application permet de s�lectionner le type de formation suivie (plein temps ou temps partiel)
 * - Il possible de choisir les semestres pour lesquels on d�sire consulter le plan modulaire
 * - On peut alors afficher la liste des modules correspondant aux choix effectu�s
 * - La s�lection d�un module permet d�afficher la liste des unit�s de cours dispens�es dans ce module
 * 
 * @author Jonathan Blum - Num�ro du poste: HEG-WS-8139
 * @version Version 1.0
 */
public class MainActivity extends Activity {

  private static final int ETAT_INITIAL = 0;
  private static final int ETAT_LISTE = 1;
  private static final int ETAT_DETAIL = 2;
  
  private static final int NB_SEM_PLEINTEMPS = 6;
  
  private static final int MODE_PLEINTEMPS = 0;
  private static final int MODE_TEMPSPARTIEL = 1;
  
  private int mode = MODE_PLEINTEMPS;
  
  private static final String[] FROM = {"nummodule", "nommodule", "credits", "semestre", "nbheures"};
  private static final int[] TO = {R.id.tvNumeroM, R.id.tvNomMod, R.id.tvCredits, R.id.tvSemestre, R.id.tvNbHeures};
  
  private static final String MODULE = "module";
  l0
  private ArrayList<CheckBox> ckAll = new ArrayList<CheckBox>();
  private ArrayList<TextView> arCours = new ArrayList<TextView>();
  private ArrayList<TextView> arHeures = new ArrayList<TextView>();
  
  yh0y0
  private HashMap<String, Module> listeModules;
  private HashMap<Integer, TreeSet<Module>> listeModulesBySemestresPT = new HashMap<Integer, TreeSet<Module>>();
  private HashMap<Integer, TreeSet<Module>> listeModulesBySemestresTP = new HashMap<Integer, TreeSet<Module>>();
  private HashMap<String,TreeSet<UniteCours>> listeUC;
  
  /* Contr�les de l'application */
  private RadioGroup rgTypeFormation;
  private RadioButton rbPleinTemps, rbTempsPartiel;
  private CheckBox ck1, ck2, ck3, ck4, ck5, ck6, ck7, ck8;
  private Button btnFiltrer;
  private LinearLayout layDetailModule, layListe;
  private TextView tvDetailModule, tvHeures1, tvCours1, tvHeures2, tvCours2, tvHeures3, tvCours3;
  private ListView lvModules;
  
  private void definirVariables () {
    rgTypeFormation = (RadioGroup)findViewById(R.id.rgTypeFormation);
    rbPleinTemps = (RadioButton)findViewById(R.id.rbPleinTemps);
    rbTempsPartiel = (RadioButton)findViewById(R.id.rbTempsPartiel);
    ck1 = (CheckBox)findViewById(R.id.ck1); ckAll.add(ck1);
    ck2 = (CheckBox)findViewById(R.id.ck2); ckAll.add(ck2);
    ck3 = (CheckBox)findViewById(R.id.ck3); ckAll.add(ck3);
    ck4 = (CheckBox)findViewById(R.id.ck4); ckAll.add(ck4);
    ck5 = (CheckBox)findViewById(R.id.ck5); ckAll.add(ck5);
    ck6 = (CheckBox)findViewById(R.id.ck6); ckAll.add(ck6);
    ck7 = (CheckBox)findViewById(R.id.ck7); ckAll.add(ck7);
    ck8 = (CheckBox)findViewById(R.id.ck8); ckAll.add(ck8);
    btnFiltrer = (Button)findViewById(R.id.btnFiltrer);
    layDetailModule = (LinearLayout)findViewById(R.id.layDetailModule);
    tvDetailModule = (TextView)findViewById(R.id.tvDetailModule);
    tvHeures1 = (TextView)findViewById(R.id.tvHeures1);
    tvCours1 = (TextView)findViewById(R.id.tvCours1);
    tvHeures2 = (TextView)findViewById(R.id.tvHeures2);
    tvCours2 = (TextView)findViewById(R.id.tvCours2);
    tvHeures3 = (TextView)findViewById(R.id.tvHeures3);
    tvCours3 = (TextView)findViewById(R.id.tvCours3);
    layListe = (LinearLayout)findViewById(R.id.layListe);
    lvModules = (ListView)findViewById(R.id.lvModules);
    
    
    arCours = new ArrayList<TextView>();
    arCours.add(tvCours1); arHeures.add(tvHeures1); 
    arCours.add(tvCours2); arHeures.add(tvHeures2);
    arCours.add(tvCours3); arHeures.add(tvHeures3);
  } // definirVariables
  
  private void definirListeners() {
    lvModules.setOnItemClickListener(new OnItemClickListener() {
      public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
        @SuppressWarnings("unchecked") HashMap<String, Object> hm = (HashMap<String, Object>)parent.getItemAtPosition(position);
        afficherDetail((Module)hm.get(MODULE));
      } // onItemClick
    });
  }
  private void initialiser() {
    Resources res = getResources();
    rbPleinTemps.setChecked(true);
    btnFiltrer.setEnabled(false);
    listeModules = Dao.getModules(res);
    getModulesBySemestres();
    listeUC = Dao.getUnitecours(res);
    setMode(MODE_PLEINTEMPS);
    setEtat(ETAT_INITIAL);
  }
  
  private void getModulesBySemestres() {
    for(Module m: listeModules.values()) {
      //PT
      if(listeModulesBySemestresPT.containsKey(m.getSemPleinTemps())) {
        listeModulesBySemestresPT.get(m.getSemPleinTemps()).add(m);
      } else {
        TreeSet<Module> tsPT= new TreeSet<Module>();
        tsPT.add(m);
        listeModulesBySemestresPT.put(m.getSemPleinTemps(), tsPT);
      }
      
      
      if(listeModulesBySemestresTP.containsKey(m.getSemTempsPartiel())) {
        listeModulesBySemestresTP.get(m.getSemTempsPartiel()).add(m);
      } else {
        TreeSet<Module> tsTP= new TreeSet<Module>();
        tsTP.add(m);
        listeModulesBySemestresTP.put(m.getSemTempsPartiel(), tsTP);
      }     
        
    }
  }
    
  @Override
  protected void onCreate (Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    definirVariables();
    definirListeners();
    initialiser();
  } // onCreate
  
  public void onCheckboxClicked(View view) {
    checkEtatBtFiltrer();
    setEtat(ETAT_LISTE);
  }
  
  public void onRadioButtonClicked(View view) {
    setMode(rbPleinTemps.isChecked()?MODE_PLEINTEMPS:MODE_TEMPSPARTIEL);
    checkEtatBtFiltrer();
    setEtat(ETAT_LISTE);
  }
  
  public void filtrer(View view) { 
    TreeSet<Module> tsFiltre = new TreeSet<Module>();
    for(int i=0; i<ckAll.size(); i++) {
      if(ckAll.get(i).isChecked() && ckAll.get(i).getVisibility() == View.VISIBLE) {
        tsFiltre.addAll((mode == MODE_PLEINTEMPS) ? listeModulesBySemestresPT.get(i+1): listeModulesBySemestresTP.get(i+1));
      }
    }
    afficherModules(tsFiltre);
    }
  public void afficherModules(TreeSet<Module> modules) {
    List<HashMap<String, Object>> data = new ArrayList<HashMap<String,Object>>(modules.size());
    for(Module m: modules) {
      Log.d("Module", m.getNom());
      HashMap<String, Object> map = new HashMap<String, Object>();
      map.put(FROM[0], m.getNumero());
      map.put(FROM[1], m.getNom());
      map.put(FROM[2], Integer.toString(m.getCredits()));
      map.put(FROM[3], Integer.toString((mode == MODE_PLEINTEMPS)?m.getSemPleinTemps():m.getSemTempsPartiel()));
      int nbheures = 0;
      for(UniteCours uc: listeUC.get(m.getNumero()))
        nbheures += uc.getHeures();
      map.put(FROM[4], Integer.toString(nbheures));
      map.put(MODULE, m);
      data.add(map);      
    }
    SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), data, R.layout.un_module, FROM, TO);
    lvModules.setAdapter(adapter);
    setEtat(ETAT_LISTE);
  }
  
  private void checkEtatBtFiltrer() {
    boolean isSomethingChecked = false;
    for(CheckBox ck: ckAll)
      if(ck.getVisibility() == View.VISIBLE  && ck.isChecked()) isSomethingChecked = true;    
    btnFiltrer.setEnabled(isSomethingChecked);
  }
  public void afficherDetail(Module m) {
    for(TextView tv: arCours)
      tv.setVisibility(View.GONE);
    for(TextView tv: arHeures)
      tv.setVisibility(View.GONE);
    tvDetailModule.setText(getString(R.string.libDetail) + m.getNumero());
    ArrayList<UniteCours> cours = new ArrayList<UniteCours>(listeUC.get(m.getNumero()));
    for(UniteCours uc: cours)
      Log.d("CoursHeure", uc.getNom());

    
    for (int i = 0; i < cours.size(); i++) {
      arCours.get(i).setText(cours.get(i).getNom());
      //arHeures.get(i).setText(Integer.valueOf(cours.get(i).getHeures()));
      arCours.get(i).setVisibility(View.VISIBLE);
      //arHeures.get(i).setVisibility(View.VISIBLE);
      
    }
    
    setEtat(ETAT_DETAIL);
  }
  
  private void setMode(int mode) {
    this.mode = mode;
    for (int i = ckAll.size(); i > NB_SEM_PLEINTEMPS; i--)
      ckAll.get(i-1).setVisibility((mode==MODE_TEMPSPARTIEL)?View.VISIBLE:View.INVISIBLE);
  }
  
  public void retourListe(View view) {
    setEtat(ETAT_LISTE);
  }
  
  private void setEtat(int etat) {
    switch (etat) {
      case ETAT_INITIAL:
        layListe.setVisibility(View.GONE); layDetailModule.setVisibility(View.GONE);
        break;
      case ETAT_LISTE:
        layListe.setVisibility(View.VISIBLE); layDetailModule.setVisibility(View.GONE);
        break;
      case ETAT_DETAIL:
        layListe.setVisibility(View.GONE); layDetailModule.setVisibility(View.VISIBLE);
        break;
      default:
        setEtat(ETAT_INITIAL);
        break;
    }
  }

} // MainActivity
