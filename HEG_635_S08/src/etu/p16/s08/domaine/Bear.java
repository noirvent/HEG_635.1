/**
 * 
 */
package etu.p16.s08.domaine;

import java.io.Serializable;

/**
 * @author Jonathan Blum
 *
 */
public class Bear implements Comparable<Bear>, Serializable {
	
	private static final long serialVersionUID = 7825499445403448725L;
	private int id;
	private String nom;
	private int taille;
	private double prix;
	private int imgSmall;
	private int imgLarge;

	public Bear (int id, String nom, int taille, double prix, int imgSmall, int imgLarge){
		this.id = id;
		this.nom = nom;
		this.taille = taille;
		this.prix = prix;
		this.imgSmall = imgSmall;
		this.imgLarge = imgLarge;
	} // constructeur

	public int getId() {return id;}
	public String getNom() {return nom;}
	public int getTaille() {return taille;}
	public double getPrix() {return prix;}
	public int getImgSmall() {return imgSmall;}
	public int getImgLarge() {return imgLarge;}
	
	@Override
	public int compareTo(Bear b) {
		int res = nom.compareTo(b.nom);
		return (res != 0) ? res : id - b.id;
	} // compareTo
}
