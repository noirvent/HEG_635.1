package etu.p16.s08.presentation;

import java.io.Serializable;
import java.util.TreeSet;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import etu.p16.s08.R;
import etu.p16.s08.base.Dao;
import etu.p16.s08.domaine.Bear;
import etu.p16.s08.metier.BearsList;

/**
 * Activité principale
 *
 * @author Jonathan Blum - HEG-Genève
 * @version 1.0
 */
public class BearsActivity extends Activity {
	public static final int FILTER_BELOW30 = 0, FILTER_BETWEEN3050 = 1, FILTER_MORETHAN50 = 2, FILTER_ALL = 3;
	public static final String REF_BASKET = "basket";
	public static final String REF_CURRENT_FILTER = "currentfilter";
	public static final String REF_LAST_FILTER = "lastfilter";
	public static final int REF_ACTIVITY_BASKET = 1;
	public static final String REF_SELECTED_BEAR = "selectedbear";

			
	
	private RadioButton rb30, rb3050, rb50, rbTous;
	private RadioButton allrb[];
	private RadioGroup rgPrix;
	private Button btnFiltrer;
	private ImageButton imPanier;
	private EditText etPanier;
	private ListView lvOurs;
	private LinearLayout layListeOurs;
	private LinearLayout layUnOurs;
	private ImageView imListePhoto;
	private TextView tvDetailNom, tvDetailPrix, tvDetailTaille;
	
	private BearsList bears;
	private TreeSet<Bear> tsBasket = new TreeSet<Bear>();
	private int currentFilter = BearsActivity.FILTER_ALL;
	private int lastCheckedFilter;
	private Bear lastSelectedBear;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bears);
		definirControles();
		definirListeners();
		bears = new BearsList(Dao.getBears(getResources()));
		if(savedInstanceState == null) {
			setInitialState();
		} else {
			lastCheckedFilter = savedInstanceState.getInt(REF_LAST_FILTER);
			tsBasket = (TreeSet<Bear>) savedInstanceState.getSerializable(REF_BASKET);
			lastSelectedBear = (Bear) savedInstanceState.getSerializable(REF_SELECTED_BEAR);
			filter(savedInstanceState.getInt(REF_CURRENT_FILTER));
			if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
				if(lastSelectedBear != null)
					showBearDetail(lastSelectedBear);
				else
					layUnOurs.setVisibility(View.INVISIBLE);
			}
		}
		
		
	} // onCreate

	private void definirControles() {
		rb30 = (RadioButton)findViewById(R.id.rb30);
		rb3050 = (RadioButton)findViewById(R.id.rb3050);
		rb50 = (RadioButton)findViewById(R.id.rb50);
		rbTous = (RadioButton)findViewById(R.id.rbTous);
		allrb = new RadioButton[] {rb30, rb3050, rb50, rbTous};
		rgPrix=(RadioGroup)findViewById(R.id.radioGroup1);
		btnFiltrer = (Button)findViewById(R.id.btnFiltrer);
		imPanier = (ImageButton)findViewById(R.id.imPanier);
		etPanier = (EditText)findViewById(R.id.etPanier);
		lvOurs = (ListView)findViewById(R.id.lvOurs);
		layListeOurs = (LinearLayout)findViewById(R.id.layListeOurs);
		layUnOurs = (LinearLayout)findViewById(R.id.layUnOurs);
		imListePhoto=(ImageView)findViewById(R.id.imListePhoto);
		tvDetailNom=(TextView)findViewById(R.id.tvDetailNom);
		tvDetailPrix=(TextView)findViewById(R.id.tvDetailPrix);
		tvDetailTaille=(TextView)findViewById(R.id.tvDetailTaille);
	} // definirControles
	
	private void definirListeners() {
		rgPrix.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override public void onCheckedChanged(RadioGroup group, int checkedId) {
				btnFiltrer.setEnabled(rgPrix.getCheckedRadioButtonId() != lastCheckedFilter);
			}
		});
		
		lvOurs.setOnItemClickListener(new OnItemClickListener() {
			@Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				selectBear(view, position);
			}
		});
		
		lvOurs.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				return selectBearDetail(position);
			}
		});
		
		btnFiltrer.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) {
				selectFilter();
			}
		});
		
		imPanier.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) {
				showBasket();
			}
		});
	}

	private void selectBear(View view, int position) {
		Bear b = bears.getBear(position);
		CheckBox c = (CheckBox)view.findViewById(R.id.cbPanier);
		c.setChecked(!c.isChecked());
		if(c.isChecked()) 
			tsBasket.add(b);
		else 
			tsBasket.remove(b);
		etPanier.setText(String.valueOf(tsBasket.size()));
	}
	
	private void setInitialState() {
		rbTous.setChecked(true);
		currentFilter = BearsActivity.FILTER_ALL;
		btnFiltrer.setEnabled(false);
		showBearsList();
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			layUnOurs.setVisibility(View.INVISIBLE);
		}
	}
	
	private void showBearsList() {
		etPanier.setText(Integer.toString(tsBasket.size()));
		bears.showBearsList(getResources(), getApplicationContext(), currentFilter, tsBasket);
		lvOurs.setAdapter(bears.getAdapter());
	}
	
	protected void showBearDetail(Bear b) {
		imListePhoto.setImageResource(b.getImgLarge());
		tvDetailNom.setText(b.getNom());
		tvDetailPrix.setText(String.valueOf(b.getPrix()) + getResources().getString(R.string.libCHF));
		tvDetailTaille.setText(String.valueOf(b.getTaille()) + getResources().getString(R.string.libCm));
		layUnOurs.setVisibility(View.VISIBLE);
	}
	
	private boolean selectBearDetail(int position) {
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			Bear b = bears.getBear(position);
			showBearDetail(b);
			lastSelectedBear = b;
		}
		return true;
	}
	
	private void filter(int filter) {
		currentFilter = filter;
		showBearsList();
	}
	
	private void selectFilter() {
		btnFiltrer.setEnabled(false);
		lastCheckedFilter = rgPrix.getCheckedRadioButtonId();
		
		if(rgPrix.getCheckedRadioButtonId() == rb30.getId()) filter(BearsActivity.FILTER_BELOW30);
		else if (rgPrix.getCheckedRadioButtonId() == rb3050.getId()) filter(BearsActivity.FILTER_BETWEEN3050);
		else if (rgPrix.getCheckedRadioButtonId() == rb50.getId()) filter(BearsActivity.FILTER_MORETHAN50);
		else filter(BearsActivity.FILTER_ALL);

	}
	
	private void showBasket() {
		Intent intent = new Intent(getApplicationContext(), PanierActivity.class);
		intent.putExtra(REF_BASKET, tsBasket) 
			  .putExtra(REF_CURRENT_FILTER, currentFilter);
		startActivityForResult(intent, REF_ACTIVITY_BASKET);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(REF_CURRENT_FILTER, currentFilter);
		outState.putInt(REF_LAST_FILTER, lastCheckedFilter);
		outState.putSerializable(REF_SELECTED_BEAR, lastSelectedBear);
		outState.putSerializable(REF_BASKET, tsBasket);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("OnActivityresult", "Basket should be updated");
		//super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case RESULT_OK:
			tsBasket = (TreeSet<Bear>)data.getSerializableExtra(REF_BASKET);
			filter(data.getIntExtra(REF_CURRENT_FILTER, FILTER_ALL));

			break;
		case RESULT_CANCELED:
			break;
		default:
			break;
		}
	}	
	
} // BearsActivity
