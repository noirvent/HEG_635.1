package etu.p16.s08.presentation;

import java.util.TreeSet;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import etu.p16.s08.R;
import etu.p16.s08.domaine.Bear;

/**
 * Activité principale
 *
 * @author Jonathan Blum - 635.1 - HEG-Genève
 * @version 1.0
 */
public class PanierActivity extends Activity {

	private EditText etNbPieces, etSommeTotale;
	private ImageButton imDelete;
	private ImageView imPhotoPanier;
	private ListView lvPanier;
	private LinearLayout layUnOursPanier;
	private TextView tvDetailNomPanier, tvDetailPrixPanier, tvDetailTaillePanier;

	private List<HashMap<String, Object>> data;
	private ArrayList<Bear> deletedBears = new ArrayList<Bear>();
	private HashMap<String, Object> hmOurs;
	private TreeSet<Bear> basket;
	private double total;
	private int currentfilter = BearsActivity.FILTER_ALL;
	private Bear lastSelectedBear;
	private Intent intent = new Intent();
	private DecimalFormat decimalformat = new DecimalFormat();

	private static final String REF_DELETED_BEARS = "deletedbears";
	private static final String REF_BEAR = "bear";
	private static final String[] FROM = { "peluche", "taille", "prix", "coche"};
	private static final int[] TO = { R.id.tvPeluchePanier, R.id.tvTaillePanier, R.id.tvPrixPanier, R.id.cbSuppression};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_panier);

		definirVariables();
		definirListeners();
		if(savedInstanceState == null) 
			setInitialState();
		else{
			currentfilter = savedInstanceState.getInt(BearsActivity.REF_CURRENT_FILTER);
			basket = (TreeSet<Bear>) savedInstanceState.getSerializable(BearsActivity.REF_BASKET);
			showBasket();
			lastSelectedBear = (Bear) savedInstanceState.getSerializable(BearsActivity.REF_SELECTED_BEAR);
			if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
				if(lastSelectedBear != null)
					showBearDetail(lastSelectedBear);
				else
					layUnOursPanier.setVisibility(View.INVISIBLE);
			}
		}
	} // onCreate	

	@Override
	public void onBackPressed() {
		Log.d("OnBack", "Should send results");
		//super.onBackPressed();
		intent.putExtra(BearsActivity.REF_BASKET, basket);
		intent.putExtra(BearsActivity.REF_CURRENT_FILTER, currentfilter);
		setResult(RESULT_OK, intent);
		finish();
	}

	private void showBasket() {
		total = 0;
		data = new ArrayList<HashMap<String,Object>>();
		for(Bear b: basket) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(FROM[0], b.getNom());
			map.put(FROM[1], b.getTaille() + getString(R.string.libCm));
			map.put(FROM[2], b.getPrix() + getString(R.string.libCHF));
			map.put(FROM[3], deletedBears.contains(b));
			map.put(REF_BEAR, b);
			data.add(map);
			total += b.getPrix();			
		}
		SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), data, R.layout.un_ours_panier, FROM, TO);
		lvPanier.setAdapter(adapter);
		etNbPieces.setText(Integer.toString(basket.size()));
		etSommeTotale.setText(decimalformat.format(total) + getString(R.string.libCHF));
	}


	private void setInitialState() {
		basket = (TreeSet<Bear>) getIntent().getSerializableExtra(BearsActivity.REF_BASKET);
		//currentfilter = getIntent().getIntExtra(REF_FILTER, BearsActivity.FILTER_ALL);
		decimalformat.setMinimumFractionDigits(2);
		decimalformat.setMaximumFractionDigits(2);
		showBasket();
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			layUnOursPanier.setVisibility(View.INVISIBLE);
	}


	private void definirVariables(){
		etNbPieces = (EditText)findViewById(R.id.etNbPieces);
		etSommeTotale = (EditText)findViewById(R.id.etSommeTotale);
		lvPanier = (ListView)findViewById(R.id.lvPanier);
		layUnOursPanier = (LinearLayout)findViewById(R.id.layUnOursPanier);
		tvDetailNomPanier = (TextView)findViewById(R.id.tvDetailNomPanier);
		tvDetailPrixPanier = (TextView)findViewById(R.id.tvDetailPrixPanier);
		tvDetailTaillePanier = (TextView)findViewById(R.id.tvDetailTaillePanier);
		imDelete = (ImageButton)findViewById(R.id.imDelete);
		imPhotoPanier = (ImageView)findViewById(R.id.imPhotoPanier);		
	} // definirVariables

	private void definirListeners() {
		lvPanier.setOnItemClickListener(new OnItemClickListener() {
			@Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				selectBear(parent, view, position);	
			}
		});

		lvPanier.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				return selectBearDetail(parent, position);
			}
		});
		
		imDelete.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) {
				removeSelectedBears();
			}
		});
	}

	protected void showBearDetail(Bear b) {
		imPhotoPanier.setImageResource(b.getImgLarge());
		tvDetailNomPanier.setText(b.getNom());
		tvDetailPrixPanier.setText(String.valueOf(b.getPrix()) + getResources().getString(R.string.libCHF));
		tvDetailTaillePanier.setText(String.valueOf(b.getTaille()) + getResources().getString(R.string.libCm));
		layUnOursPanier.setVisibility(View.VISIBLE);
	}

	private void selectBear(AdapterView<?> parent, View view, int position) {
		hmOurs = (HashMap<String, Object>)parent.getItemAtPosition(position);
		Bear currentBear = (Bear) hmOurs.get(REF_BEAR);
		CheckBox ck = (CheckBox)view.findViewById(R.id.cbSuppression);
		ck.setChecked(!ck.isChecked());
		if(ck.isChecked())
			deletedBears.add(currentBear);
		else
			deletedBears.remove(currentBear);
	}

	private boolean selectBearDetail(AdapterView<?> parent, int position) {
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			HashMap<String, Object> hm = (HashMap<String, Object>)parent.getItemAtPosition(position);
			Bear b = (Bear)hm.get(REF_BEAR);
			showBearDetail(b);
			lastSelectedBear = b;
		}
		return true;
	}
	
	private void removeSelectedBears() {
		basket.removeAll(deletedBears);
		deletedBears.clear();
		showBasket();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(BearsActivity.REF_CURRENT_FILTER, currentfilter);
		outState.putSerializable(BearsActivity.REF_SELECTED_BEAR, lastSelectedBear);
		outState.putSerializable(BearsActivity.REF_BASKET, basket);
		outState.putSerializable(REF_DELETED_BEARS, deletedBears);
	}


} // PanierActivity
