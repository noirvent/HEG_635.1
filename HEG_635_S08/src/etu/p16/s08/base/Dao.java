package etu.p16.s08.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.TreeSet;

import android.content.res.Resources;
import etu.p16.s08.R;
import etu.p16.s08.domaine.Bear;

/**
 * Database Access Objets
 *
 * @author Jonathan Blum - 635.1 - HEG-Genève
 * @version 1.0
 */
public class Dao {

	private static final int EOF = -1;                    /* Marque de fin de fichier */

	/* Retourne la référence de R.drawable de l'image de nom nomImage */
	private static int refImage (Resources res, String nomImage, boolean large) {
		if (large) {return res.getIdentifier(nomImage.toLowerCase()+1, "drawable", R.class.getPackage().getName());} 
		else {return res.getIdentifier(nomImage.toLowerCase()+0, "drawable", R.class.getPackage().getName());} 
	} // refImage 

	/* Lecture de l'InputStream in et retour de la séquence de charactères dans un String */
	private static String lireStr (InputStream in) {
		StringBuilder b = new StringBuilder();
		try {
			b.ensureCapacity(in.available() + 10);
			int c = in.read();
			while (c != EOF) {b.append((char)c); c = in.read();}
			in.close();
		}
		catch (IOException e) {e.printStackTrace();}
		return b.toString();
	} // lireStr
	
	/* Retourne la liste des ours dans l'ordre alphabétique des noms */
	  public static TreeSet<Bear> getBears (Resources res) {
	    TreeSet<Bear> bear = new TreeSet<Bear>();
	    String[] bearsStr = lireStr(res.openRawResource(R.raw.bears)).split("\r\n");
	    for (int k = 0; k < bearsStr.length; k++) {
	      String[] val = bearsStr[k].split(";");
	      int refImgSmall = refImage(res, val[1], false);
	      int refImgGrand = refImage(res, val[1], true);
	      bear.add(new Bear(Integer.parseInt(val[0]), val[1], Integer.parseInt(val[2]), Double.parseDouble(val[3]), refImgSmall, refImgGrand));
	    }
	    return bear;
	  } // getBears
	  
}
