/**
 * 
 */
package etu.p16.s08.metier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import android.content.Context;
import android.content.res.Resources;
import android.widget.SimpleAdapter;
import etu.p16.s08.R;
import etu.p16.s08.domaine.Bear;

/**
 * @author Jonathan Blum
 *
 */
public class BearsList {
	public static final int BELOW30 = 0, BETWEEN3050 = 1, MORETHAN50 = 2, ALL = 3;
	
	private static final String[] FROM = {"photo", "nom", "taille", "prix", "checked"};
	private static final int [] TO = {R.id.imOurs, R.id.tvNom, R.id.tvTaille, R.id.tvPrix, R.id.cbPanier};
	private final String REF_BEAR = "Bear";
	
	private TreeSet<Bear> bears = new TreeSet<Bear>();
	private List<TreeSet<Bear>> bearsList;
	private List<HashMap<String, Object>> data;
	private SimpleAdapter adapter;
	
	public SimpleAdapter getAdapter() { return adapter; }
	public Bear getBear(int pos) {return (Bear)data.get(pos).get(REF_BEAR);}
	
	
	public BearsList(TreeSet<Bear> bears) {
		this.bears = bears;
		initialize();
	}
	
	public void showBearsList(Resources res, Context context, int filter, TreeSet<Bear> basket) {
		data = new ArrayList<HashMap<String,Object>>(bearsList.get(filter).size());
		for(Bear b: bearsList.get(filter)) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(FROM[0], b.getImgSmall());
			map.put(FROM[1], b.getNom());
			map.put(FROM[2], b.getTaille() + res.getString(R.string.libCm));
			map.put(FROM[3], b.getPrix() + res.getString(R.string.libCHF));
			map.put(FROM[4], basket.contains(b));
			map.put(REF_BEAR, b);
			data.add(map);
		}
		adapter = new SimpleAdapter(context, data, R.layout.un_ours, FROM, TO);
	}
	
	private void initialize() {
		bearsList = new ArrayList<TreeSet<Bear>>();
		bearsList.add(BearsList.BELOW30, new TreeSet<Bear>());
		bearsList.add(BearsList.BETWEEN3050, new TreeSet<Bear>());
		bearsList.add(BearsList.MORETHAN50, new TreeSet<Bear>());
		bearsList.add(BearsList.ALL, bears);
		
		for(Bear b: bears) {
			if(b.getPrix() < 30)
				bearsList.get(BearsList.BELOW30).add(b);
			else if (30 <= b.getPrix() && b.getPrix() < 50)
				bearsList.get(BearsList.BETWEEN3050).add(b);
			else
				bearsList.get(BearsList.MORETHAN50).add(b);
		}
	}
	
}
