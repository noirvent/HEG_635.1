package etu.p16.cc1.plan.modulaire.presentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import etu.p16.cc1.plan.modulaire.R;
import etu.p16.cc1.plan.modulaire.base.Dao;
import etu.p16.cc1.plan.modulaire.domaine.Module;
import etu.p16.cc1.plan.modulaire.domaine.UniteCours;

/**
 * 635.1 - Contr�le continu du 15.04.2016
 *
 * Application de consultation du plan modulaire de la HEG
 * - L'application permet de s�lectionner le type de formation suivie (plein temps ou temps partiel)
 * - Il possible de choisir les semestres pour lesquels on d�sire consulter le plan modulaire
 * - On peut alors afficher la liste des modules correspondant aux choix effectu�s
 * - La s�lection d�un module permet d�afficher la liste des unit�s de cours dispens�es dans ce module
 * 
 * @author Blum Jonathan
 * @version Version 2.0
 */
public class MainActivity extends Activity {

	private static final int ETAT_INITIAL = 0;
	private static final int ETAT_LISTE = 1;
	private static final int ETAT_DETAIL = 2;

	private HashMap<String, Module> hmModules = new HashMap<String, Module>();
	private HashMap<Integer, TreeSet<Module>> hmModulesPleinTemps = new HashMap<Integer, TreeSet<Module>>();
	private HashMap<Integer, TreeSet<Module>> hmModulesTempsPartiel = new HashMap<Integer, TreeSet<Module>>();


	private static final String MODULE = "module";

	private static final String[] FROM = {"nummodule", "nommodule", "credits", "semestre", "heures"};
	private static final int[] TO = {R.id.tvNumeroM, R.id.tvNomMod, R.id.tvCredits , R.id.tvSemestre , R.id.tvNbHeures};

	private ArrayList<CheckBox> ckAll = new ArrayList<CheckBox>();
	private ArrayList<TextView> arCours = new ArrayList<TextView>();
	private ArrayList<TextView> arHeures = new ArrayList<TextView>();

	/* Contr�les de l'application */
	private RadioGroup rgTypeFormation;
	private RadioButton rbPleinTemps, rbTempsPartiel;
	private CheckBox ck1, ck2, ck3, ck4, ck5, ck6, ck7, ck8;
	private Button btnFiltrer;
	private LinearLayout layDetailModule, layListe;
	private TextView tvDetailModule, tvHeures1, tvCours1, tvHeures2, tvCours2, tvHeures3, tvCours3;
	private ListView lvModules;

	private void definirVariables () {
		rgTypeFormation = (RadioGroup)findViewById(R.id.rgTypeFormation);
		rbPleinTemps = (RadioButton)findViewById(R.id.rbPleinTemps);
		rbTempsPartiel = (RadioButton)findViewById(R.id.rbTempsPartiel);
		ck1 = (CheckBox)findViewById(R.id.ck1); ckAll.add(ck1);
		ck2 = (CheckBox)findViewById(R.id.ck2); ckAll.add(ck2);
		ck3 = (CheckBox)findViewById(R.id.ck3); ckAll.add(ck3);
		ck4 = (CheckBox)findViewById(R.id.ck4); ckAll.add(ck4);
		ck5 = (CheckBox)findViewById(R.id.ck5); ckAll.add(ck5);
		ck6 = (CheckBox)findViewById(R.id.ck6); ckAll.add(ck6);
		ck7 = (CheckBox)findViewById(R.id.ck7); ckAll.add(ck7);
		ck8 = (CheckBox)findViewById(R.id.ck8); ckAll.add(ck8);
		btnFiltrer = (Button)findViewById(R.id.btnFiltrer);
		layDetailModule = (LinearLayout)findViewById(R.id.layDetailModule);
		tvDetailModule = (TextView)findViewById(R.id.tvDetailModule);
		tvHeures1 = (TextView)findViewById(R.id.tvHeures1);
		tvCours1 = (TextView)findViewById(R.id.tvCours1);
		tvHeures2 = (TextView)findViewById(R.id.tvHeures2);
		tvCours2 = (TextView)findViewById(R.id.tvCours2);
		tvHeures3 = (TextView)findViewById(R.id.tvHeures3);
		tvCours3 = (TextView)findViewById(R.id.tvCours3);
		layListe = (LinearLayout)findViewById(R.id.layListe);
		lvModules = (ListView)findViewById(R.id.lvModules);

		arCours.add(tvCours1); arHeures.add(tvHeures1);
		arCours.add(tvCours2); arHeures.add(tvHeures2);
		arCours.add(tvCours3); arHeures.add(tvHeures3);

	} // definirVariables

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		definirVariables();
		definirListener();
		definirDonnees();
		initialisation();

	} // onCreate

	private void definirDonnees() {
		Resources res = getResources();
		hmModules = Dao.getModules(res);
		Dao.getUniteCours(res, hmModules);
		filterModulesBySemestres();
	}

	private void definirListener() {
		lvModules.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				@SuppressWarnings("unchecked") HashMap<String, Object> hm = (HashMap<String, Object>)parent.getItemAtPosition(position);
				afficherDetail((Module)hm.get(MODULE));
			}
		});
	}

	private void initialisation() {
		setEtat(ETAT_INITIAL);
		rbPleinTemps.setChecked(true);
		btnFiltrer.setEnabled(false);
		toggleTempsPartiel();
	}

	public void retourListe(View view) {
		setEtat(ETAT_LISTE);
	}

	private void setEtat(int etat) {
		switch(etat) {
		case ETAT_INITIAL:
			layListe.setVisibility(View.GONE); layDetailModule.setVisibility(View.GONE);
			break;
		case ETAT_LISTE:
			layListe.setVisibility(View.VISIBLE); layDetailModule.setVisibility(View.GONE);
			break;
		case ETAT_DETAIL:
			layListe.setVisibility(View.GONE); layDetailModule.setVisibility(View.VISIBLE);
			break;
		default:
			setEtat(ETAT_INITIAL); break;
		}
	}

	private void toggleTempsPartiel() {
		if(rbTempsPartiel.isChecked()) {
			ck7.setVisibility(CheckBox.VISIBLE);
			ck8.setVisibility(CheckBox.VISIBLE);
		} else {
			ck7.setVisibility(CheckBox.INVISIBLE);
			ck8.setVisibility(CheckBox.INVISIBLE);		  
		}
	}

	private void filterModulesBySemestres() {
		for(Module m: hmModules.values()) {
			if(hmModulesPleinTemps.containsKey(m.getSemPleinTemps())) {
				hmModulesPleinTemps.get(m.getSemPleinTemps()).add(m);
			} else {
				TreeSet<Module> tsPleinTemps = new TreeSet<Module>();
				tsPleinTemps.add(m);
				hmModulesPleinTemps.put(m.getSemPleinTemps(), tsPleinTemps);
			}

			if(hmModulesTempsPartiel.containsKey(m.getSemTempsPartiel())) {
				hmModulesTempsPartiel.get(m.getSemTempsPartiel()).add(m);
			} else {
				TreeSet<Module> tsTempsPartiel = new TreeSet<Module>();
				tsTempsPartiel.add(m);
				hmModulesTempsPartiel.put(m.getSemTempsPartiel(), tsTempsPartiel);
			}
		}
	}

	private TreeSet<Module> getSelectedSemesters() {
		TreeSet<Module> modules = new TreeSet<Module>();
		for(int i=0; i<ckAll.size(); i++) {
			if(ckAll.get(i).isChecked() && ckAll.get(i).getVisibility() == CheckBox.VISIBLE) {
				modules.addAll((rbPleinTemps.isChecked()) ? hmModulesPleinTemps.get(i+1): hmModulesTempsPartiel.get(i+1));
			}
		}
		return modules;
	}

	public void afficherListe(View view) {
		btnFiltrer.setEnabled(false);

		TreeSet<Module> modToDisplay  = getSelectedSemesters();

		List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>(modToDisplay.size());    

		for(Module m: modToDisplay) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(FROM[0], m.getNumero());
			map.put(FROM[1], m.getNom());
			map.put(FROM[2], m.getCredits());
			map.put(FROM[3], rbPleinTemps.isChecked() ? m.getSemPleinTemps() : m.getSemTempsPartiel());
			map.put(FROM[4], m.getTotalHeures());
			map.put(MODULE, m);
			data.add(map);
		}

		SimpleAdapter listViewAdapter = new SimpleAdapter(getApplicationContext(), data, R.layout.un_module, FROM, TO);
		lvModules.setAdapter(listViewAdapter);  

		setEtat(ETAT_LISTE);
	}

	private void afficherDetail(Module m) {
		setEtat(ETAT_DETAIL);
		for(TextView tv: arCours) tv.setVisibility(TextView.GONE);
		for(TextView tv: arHeures) tv.setVisibility(TextView.GONE);

		tvDetailModule.setText(String.format("%s %s", getString(R.string.libDetail),m.getNumero()));
		ArrayList<UniteCours> cours = m.getUniteCours();

		for(int i = 0; i < cours.size(); i++) {
			arHeures.get(i).setText(String.format("%dh", cours.get(i).getHeures()));
			arHeures.get(i).setVisibility(TextView.VISIBLE);
			arCours.get(i).setText(cours.get(i).getNom());
			arCours.get(i).setVisibility(TextView.VISIBLE);
		}		
	}

	public void onRadioButtonClicked(View view) {
		toggleTempsPartiel();
		setEtat(ETAT_INITIAL);
		toggleControles();
	}

	public void onCheckboxClicked(View view) {
		setEtat(ETAT_INITIAL);
		toggleControles();
	}

	public void toggleControles() {
		boolean isSomethingChecked = false;
		for(CheckBox ck : ckAll) {
			if(ck.getVisibility() == CheckBox.VISIBLE && ck.isChecked())
				isSomethingChecked = true;
		}
		btnFiltrer.setEnabled(isSomethingChecked);
	}

} // MainActivity
