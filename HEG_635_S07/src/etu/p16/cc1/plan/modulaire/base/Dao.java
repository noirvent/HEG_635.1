package etu.p16.cc1.plan.modulaire.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import android.content.res.Resources;
import etu.p16.cc1.plan.modulaire.R;
import etu.p16.cc1.plan.modulaire.domaine.Module;
import etu.p16.cc1.plan.modulaire.domaine.UniteCours;

/**
 * 635.1 - Contr�le continu du 15.04.2016
 *
 * Application de consultation du plan modulaire de la HEG
 * 
 * Chargement des donn�es.
 * 
 * Format des fichiers:
 * - module.txt: n�mod;nom;credits;semPT;semTP
 * - uc.txt:     n�mod;nom;nbHeures
 * 
 * @author Blum Jonathan
 * @version Version 2.0
 */
public class Dao {

	private static final int EOF = -1; /* Marque de fin de fichier */

	/* Lecture de l'InputStream in et retour de la s�quence de charact�res dans un String */
	private static String lireStr (InputStream in) {
		StringBuilder b = new StringBuilder();
		try {
			b.ensureCapacity(in.available() + 10);
			int c = in.read();
			while (c != EOF) {b.append((char)c); c = in.read();}
			in.close();
		}
		catch (IOException e) {e.printStackTrace();}
		return b.toString();
	} // lireStr

	public static HashMap<String, Module> getModules (Resources res) {
		HashMap<String, Module> hmModules = new HashMap<String, Module>();
		String[] rawModules = lireStr(res.openRawResource(R.raw.module)).split("\r\n");
		for(String line: rawModules) {
			String[] values = line.split(";");
			Module m = new Module(
					values[0],
					values[1],
					Integer.parseInt(values[2]),
					Integer.parseInt(values[3]),
					Integer.parseInt(values[4])
					);
			hmModules.put(values[0], m);
		}

		return hmModules;
	}

	public static void getUniteCours(Resources res, HashMap<String, Module> modules) {
		//TreeSet<UniteCours> uc = new TreeSet<UniteCours>();
		String[] rawUC = lireStr(res.openRawResource(R.raw.uc)).split("\r\n");
		for(String line: rawUC) {
			String[] values = line.split(";");
			UniteCours uc = new UniteCours(values[0], values[1], Integer.parseInt(values[2]));
			modules.get(values[0]).addUniteCours(uc);
			//uc.add(uc);
		}
	}



} // Dao
