package etu.p16.cc1.plan.modulaire.domaine;

import java.util.ArrayList;

/**
 * 635.1 - Contr�le continu du 15.04.2016
 *
 * Application de consultation du plan modulaire de la HEG
 * 
 * Repr�sentation d'un module.
 *
 * @author Blum Jonathan
 * @version Version 2.0
 */
public class Module implements Comparable<Module> {

	private String numero;       /* Num�ro du module: identifiant */
	private String nom;          /* Nom du module */
	private int credits;         /* Nombre de cr�dits */
	private int semPleinTemps;   /* Num�ro du semestre o� le module est donn� aux �tudiants � plein temps */
	private int semTempsPartiel; /* Num�ro du semestre o� le module est donn� aux �tudiants � temps partiel */


	private ArrayList<UniteCours> uniteCours = new ArrayList<UniteCours>(); // Liste des UC pour ce module
	private int totalHeures = 0;

	/** Constructeur */
	public Module (String numero, String nom, int credits, int semPleinTemps, int semTempsPartiel) {
		this.numero = numero; this.nom = nom; this.credits = credits;
		this.semPleinTemps = semPleinTemps; this.semTempsPartiel = semTempsPartiel;
	} // Constructeur

	public String getNumero () {return numero;}
	public String getNom () {return nom;}
	public int getCredits () {return credits;}
	public int getTotalHeures () {return totalHeures;}
	public int getSemPleinTemps () {return semPleinTemps;}
	public int getSemTempsPartiel () {return semTempsPartiel;}
	public ArrayList<UniteCours> getUniteCours() {return uniteCours;}

	public void addUniteCours(UniteCours uc) {
		uniteCours.add(uc);
		totalHeures += uc.getHeures();
	}

	@Override
	public boolean equals (Object obj) {return ((Module)obj).numero.equals(numero);}

	@Override
	/** Ordre croissant des num�ros de module */
	public int compareTo (Module m) {return numero.compareTo(m.numero);}

	@Override
	public String toString() {
		return "Module [numero=" + numero + ", nom=" + nom + ", credits=" + credits + ", semPleinTemps=" + semPleinTemps
				+ ", semTempsPartiel=" + semTempsPartiel + ", uniteCours=" + uniteCours + ", totalHeures=" + totalHeures
				+ "]";
	}


} // Module
