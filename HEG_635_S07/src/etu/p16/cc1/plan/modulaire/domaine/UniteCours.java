package etu.p16.cc1.plan.modulaire.domaine;

/**
 * 635.1 - Contr�le continu du 15.04.2016
 *
 * Application de consultation du plan modulaire de la HEG
 * 
 * Repr�sentation d'une unit� de cours.
 *
 * @author Blum Jonathan
 * @version Version 2.0
 */
public class UniteCours implements Comparable<UniteCours> {

	private String numero; /* Num�ro du module dont le cours fait partie */
	private String nom;    /* Nom du cours */
	private int heures;    /* Nombre d'heures de cours par semaine */

	/** Constructeur */
	public UniteCours (String numero, String nom, int heures) {
		this.numero = numero; this.nom = nom; this.heures = heures;
	} // Constructeur

	public String getNumero () {return numero;}
	public String getNom () {return nom;}
	public int getHeures () {return heures;}

	@Override
	public boolean equals (Object obj) {return ((UniteCours)obj).nom.equals(nom);}

	@Override
	/** Ordre croissant des nom de cours, puis du num�ro de module */
	public int compareTo (UniteCours uc) {
		int res = nom.compareTo(uc.nom);
		return (res != 0) ? res : numero.compareTo(uc.numero);
	}

	@Override
	public String toString() {
		return "UniteCours [numero=" + numero + ", nom=" + nom + "]";
	}

} // UniteCours
