package FigureGeometrique;

public class Rectangle extends FigureGeometrique {
	private float longueur;
	private float hauteur;
	public Rectangle(float longueur,float hauteur ) {
		this.longueur = longueur;
		this.hauteur = hauteur;
	}
	@Override
	public float calculeAire() {
		return longueur*hauteur;
	}

}
