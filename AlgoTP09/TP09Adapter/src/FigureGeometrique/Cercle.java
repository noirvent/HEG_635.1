package FigureGeometrique;

public class Cercle extends FigureGeometrique{
	float rayon;

	public Cercle(float rayon) {
		this.rayon = rayon;
	}

	@Override
	public float calculeAire() {
		// TODO Auto-generated method stub
		return (float) (rayon*rayon*Math.PI);
	}

}
