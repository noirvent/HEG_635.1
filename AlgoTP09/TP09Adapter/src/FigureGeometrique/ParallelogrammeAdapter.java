/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FigureGeometrique;

/**
 *
 * @author Jonathan Blum <jonathan.blum@eldhar.com>
 */
public class ParallelogrammeAdapter extends FigureGeometrique {
    Parallelogramme parallelogrammeAdaptee;

    public ParallelogrammeAdapter(float rayon) {
        parallelogrammeAdaptee = new Parallelogramme(rayon);
    }
    
    @Override
    public float calculeAire() {
        return parallelogrammeAdaptee.calculerSurfaceAir();
    }
    
}
