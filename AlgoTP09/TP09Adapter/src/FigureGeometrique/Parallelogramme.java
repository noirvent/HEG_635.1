package FigureGeometrique;

public class Parallelogramme {
	float rayon;

	public Parallelogramme(float rayon) {
		this.rayon = rayon;
	}

	public float calculerSurfaceAir() {
		// TODO Auto-generated method stub
		return (float) (rayon*rayon*Math.PI);
	}

}
