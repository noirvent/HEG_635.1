package FigureGeometrique;

import java.util.ArrayList;

public class Client {
	public static void main(String[] args){
		ArrayList<FigureGeometrique> listeFigureGeometrique = new ArrayList<>();
		
		listeFigureGeometrique.add(new Cercle(5));
		listeFigureGeometrique.add(new Cercle(5));
		listeFigureGeometrique.add(new Rectangle(5,10));
		listeFigureGeometrique.add(new ParallelogrammeAdapter(5));
		listeFigureGeometrique.add(new Cercle(5));
		listeFigureGeometrique.add(new Cercle(5));
		listeFigureGeometrique.add(new Rectangle(5,10));
		listeFigureGeometrique.add(new ParallelogrammeAdapter(5));
                
                float surfacetot = 0;
                for(FigureGeometrique f: listeFigureGeometrique)
                    surfacetot += f.calculeAire();
                
                System.out.println("Surface totale : " + surfacetot);
	}
}
