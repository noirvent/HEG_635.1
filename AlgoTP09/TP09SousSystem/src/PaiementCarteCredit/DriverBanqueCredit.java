package PaiementCarteCredit;

class DriverBanqueCredit implements BanqueCreditInterface {
	private boolean connecte = false;
	private boolean carteValide= false;
    @Override
	public boolean logging() {
			System.out.println("logging to DriverBanqueCredit");
			connecte = true;
			return connecte;
			}

    @Override
	public boolean identification(String nom,String prenom,String numero){
			if (!connecte) 
				System.out.println("non connecte");
			else { 
				System.out.println("identification: "+ nom + " " + 
				prenom + " " + numero );
				carteValide = true;
				}
			return carteValide;}

    @Override
	public boolean montant(String valeur) {
		if(!carteValide) {
			System.out.println("carte non valide"); 
			return false;
			}
		else  {
			System.out.println("valeur : " + valeur);
			carteValide = false;
			connecte = false;
			return true;
			}
		}

    @Override
	public void unlock() {System.out.println("unlocked !");}}
