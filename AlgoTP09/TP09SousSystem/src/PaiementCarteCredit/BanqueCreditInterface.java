/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaiementCarteCredit;

/**
 *
 * @author Jonathan Blum <jonathan.blum@eldhar.com>
 */
public interface BanqueCreditInterface {

    boolean identification(String nom, String prenom, String numero);

    boolean logging();

    boolean montant(String valeur);

    void unlock();
    
}
