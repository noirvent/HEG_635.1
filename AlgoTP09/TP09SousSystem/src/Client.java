

import PaiementCarteCredit.BanqueCreditFactory;
import PaiementCarteCredit.BanqueCreditInterface;

	public class Client {
	public static void main(String[] args) {
		BanqueCreditInterface banque;
		banque = new BanqueCreditFactory().getBanqueCredit();
		banque.identification("jules","cesar","1234567");
		banque.montant("1896.65");
		System.out.println("-------");
		banque.logging();
		banque.montant("1896.65");
		System.out.println("-------");
		banque.logging();
		banque.identification("jules","cesar","1234567");
		banque.montant("1896.65");}
	}


