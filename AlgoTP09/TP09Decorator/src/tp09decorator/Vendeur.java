package tp09decorator;

public class Vendeur extends Employe{
   
    private float salaire;
    
    public Vendeur (float salaire){
        this.salaire = salaire;
    }

    public float calculerSalaire() {
        return salaire;
    } 
}
