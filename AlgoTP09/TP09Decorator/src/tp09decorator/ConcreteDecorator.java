/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp09decorator;

/**
 *
 * @author Jonathan Blum <jonathan.blum@eldhar.com>
 */
public class ConcreteDecorator extends EmployeDecorator {

    public ConcreteDecorator(Employe employe) {
        super(employe);
    }
    
}
