package tp09decorator;

import java.util.*;

public class Conteneur {
	private ArrayList<Employe> contenu = new ArrayList<Employe>();
	
	public void addEmploye(Employe e){
            contenu.add(e);
	}
        
        public float calculerSalaire() {
           Iterator<Employe> it = contenu.iterator();
           float salaire = 0;
           while(it.hasNext()) {
               salaire = salaire + it.next().calculerSalaire();
           }
           return salaire;
        }
        
        public void printSalaireLevel() {
            contenu.stream().forEach((employe) -> {
                new ConcreteDecorator(employe).printSalaire();
            });
        }
}
