/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp09decorator;

/**
 *
 * @author Jonathan Blum <jonathan.blum@eldhar.com>
 */
public abstract class EmployeDecorator extends Employe {
    Employe decoratedEmploye;
    String employeType;

    public EmployeDecorator(Employe employe) {
        this.decoratedEmploye = employe;
    }
    
    @Override
    public String getNom() {return decoratedEmploye.nom;}
    
    @Override
    public void setNom(String nom) {decoratedEmploye.nom = nom;}
    
    @Override
    public String getPrenom() {return decoratedEmploye.prenom;}
    
    @Override
    public void setPrenom(String prenom) {decoratedEmploye.prenom = prenom;}
    
    @Override
    public float calculerSalaire() { return decoratedEmploye.calculerSalaire(); }
    
    public void printSalaire() {
        float  salaire = calculerSalaire();
        String typesalaire;
        if(salaire <= 5000)
            typesalaire = "bas";
        else if (5000 < salaire && salaire <= 7500)
            typesalaire = "moyen";
        else
            typesalaire = "haut";
        
        
        System.out.println(String.format("Cet employé %s a un salaire %s (%d)", decoratedEmploye.getClass().getSimpleName(), typesalaire, (int)calculerSalaire()));
    }
}
