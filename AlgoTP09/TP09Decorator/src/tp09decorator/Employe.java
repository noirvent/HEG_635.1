package tp09decorator;

public abstract class Employe {
    
    public String nom;
    public String prenom;
    
    public abstract float calculerSalaire();
    
    public String getNom() {return nom;}
    
    public void setNom(String nom) {this.nom = nom;}
    
    public String getPrenom() {return prenom;}
    
    public void setPrenom(String prenom) {this.prenom = prenom;}
    
}
