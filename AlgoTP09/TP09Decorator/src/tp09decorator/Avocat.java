package tp09decorator;

public class Avocat extends Employe{

    private float honoraire;
    private float bonusAffaireGagne;

    public Avocat(float honoraire, float bonusAffaireGagne){
        this.honoraire = honoraire;
        this.bonusAffaireGagne = bonusAffaireGagne;
    }
    
    public float calculerSalaire() {
        return honoraire + bonusAffaireGagne;
    }   
}
