package tp09decorator;

public class Test {
    
    public static void main(String[] args) {
        
        Conteneur conteneur = new Conteneur();
        
        conteneur.addEmploye(new Auditeur(4500, 1));
        conteneur.addEmploye(new Avocat(6000, 1000));
        conteneur.addEmploye(new Vendeur(5000));
        conteneur.addEmploye(new Auditeur(6000, 2));
        conteneur.addEmploye(new Vendeur(3000));
        conteneur.addEmploye(new Auditeur(3000, 3));
        conteneur.addEmploye(new Avocat(5000, 2500));
        conteneur.addEmploye(new Vendeur(4000));
        conteneur.addEmploye(new Auditeur(4500, 3));
        conteneur.addEmploye(new Vendeur(6500)); 
        
        System.out.println("La somme totale des salaires de ce mois-ci est de : " + conteneur.calculerSalaire() + " Frs.");
        
        conteneur.printSalaireLevel();
    }   
}
